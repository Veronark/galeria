<!DOCTYPE html>
    <html lang="pt-br">
        <head>
            <meta charset="utf-8">
            <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
            <meta http-equiv="x-ua-compatible" content="ie=edge">
            <title>Galeria</title>
            <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
            <link href="<?= BASEURL ?>assets/mdb/css/bootstrap.min.css" rel="stylesheet">
            <link href="<?= BASEURL ?>assets/mdb/css/mdb.min.css" rel="stylesheet">
            <link href="<?= BASEURL ?>assets/mdb/css/style.min.css" rel="stylesheet">
            <link href="<?= BASEURL ?>assets/mdb/css/a.css" rel="stylesheet">
            <link href="<?= BASEURL ?>assets/img/g.png" rel="shortcut icon">
        </head>
        <body>