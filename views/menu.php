<nav class="navbar fixed-top navbar-expand-lg navbar-light white scrolling-navbar">
    <div class="container">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item active">
                    <a class="nav-link waves-effect" href="<?= BASEURL ?>index.php">Home
                        <span class="sr-only">(current)</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link waves-effect" href="<?= BASEURL ?>pages/upload.php">Upload</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link waves-effect" href="<?= BASEURL ?>pages/galeria.php">Galeria</a>
                </li>
            </ul>
            <form class="form-inline">
                <div class="md-form my-0">
                    <input class="form-control mr-sm-2" type="text" placeholder="Pesquisar" aria-label="Pesquisar">
                </div>
            </form>
        </div>
    </div>
</nav>