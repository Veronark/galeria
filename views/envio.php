<div class="view full-page-intro" style="background-image: url('https://mdbootstrap.com/img/Photos/Others/images/33.jpg'); background-repeat: no-repeat; background-size: cover;">
    <div class="mask rgba-black-light d-flex justify-content-center align-items-center">
        <div class="container">
            <div class="row wow fadeIn justify-content-center">
                <div class="col-md-6 mt-4 mb-4">
                    <div class="card">
                        <div class="card-body">
                            <form name="envio" method="post" action="upload.php" enctype="multipart/form-data">
                                <h3 class="dark-grey-text text-center">
                                    <strong>Upload de Imagens:</strong>
                                </h3>
                                <hr>
                                <div class="md-form">
                                    <i class="fas fa-user prefix grey-text"></i>
                                    <input type="text" id="titulo" name="titulo" class="form-control">
                                    <label for="titulo">Título</label>
                                </div>
                                <div class="md-form">
                                    <i class="fas fa-list-ul prefix grey-text"></i>
                                    <input type="text" id="categoria" name="categoria" class="form-control">
                                    <label for="categoria">Categoria</label>
                                <!-- Select? -->
                                </div>
                                <div class="md-form">
                                    <i class="fas fa-pencil-alt prefix grey-text"></i>
                                    <textarea type="text" id="descricao" name="descricao" class="md-textarea"></textarea>
                                    <label for="descricao">Descrição</label>
                                </div>
                                <div class="md-form">
                                    <i class="fas fa-image prefix grey-text"></i>
                                    <input type="file" class="custom-file-input" id="arquivo" name="arquivo">
                                    <label class="custom-file-label" for="arquivo" data-browse="Pesquisar">Procurar Imagem</label>
                                </div>
                                <div class="text-center mb-4 mt-4">
                                    <button class="btn btn-dark" type="submit">Enviar</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>