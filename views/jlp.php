<div class="view full-page-intro" style="background-image: url('https://mdbootstrap.com/img/Photos/Others/images/33.jpg'); background-repeat: no-repeat; background-size: cover;">
    <div class="mask rgba-black-light d-flex justify-content-center align-items-center">
        <div class="container">   
            <div class=" mb-4 white-text text-center text-md">
                <h1 class="display-4 font-weight-bold"> Galeria de Imagens </h1>
                <hr class="hr-light">
                <p class=" ">
                    <strong>Saiba mais sobre o sistema que utilizo para armazenar minhas imagens.</strong>
                </p>
                <a href="#sistema" class="btn btn-outline-white btn-lg">  Sistema </a>
                <a target="_blank" href="http://hospedagem.ifspguarulhos.edu.br/~gu1800078/a/" class="btn btn-outline-white btn-lg"> Sobre mim </a>
            </div>
        </div>
    </div>
</div>