<main id="sistema">
    <div class="container pt-4 pb-3">
        <section>
            <h3 class="h3 text-center pt-4 mt-4 mb-4">Sobre o Sistema</h3>  
            <hr class="mb-5">
            <div class="row wow fadeIn">
                <div class="col-lg-6 col-md-12 px-4 pb-5">
                    <div class="row">
                        <div class="col-1 mr-3">
                            <i class="fas fa-lightbulb fa-2x indigo-text"></i>
                        </div>
                        <div class="col-10">
                            <h5 class="feature-title">Ideia inicial</h5>
                            <p class="grey-text">Todos nós em algum momento já tivemos problemas com falta de espaço de armazenamento, e a ideia para esse sistema seria termos um lugar para armazenamento das nossas tão amadas fotos.</p>
                        </div>
                    </div>
                    <div style="height:30px"></div>
                    <div class="row">
                        <div class="col-1 mr-3">
                            <i class="fas fa-question fa-2x blue-text"></i>
                        </div>
                        <div class="col-10">
                            <h5 class="feature-title">Como funciona?</h5>
                            <p class="grey-text">É bem simples, temos nossa página de upload onde enviaremos a imagem e está estará disponivel para ser visualizada, ou até mesmo removida, na página da galeria.</p>
                        </div>
                    </div>
                    <div style="height:30px"></div>
                    <div class="row">
                        <div class="col-1 mr-3">
                            <i class="fas fa-at fa-2x cyan-text"></i>
                        </div>
                        <div class="col-10">
                            <h5 class="feature-title">Se interessou?</h5>
                            <p class="grey-text">Caso tenha se interessado pelo sistema entre em contato por meio das nossas redes socias, encontradas no final dessa página.</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 mb-4 mt-4">
                    <img src="<?= BASEURL ?>assets/img/c.jpg" class="img-fluid z-depth-1-half" alt="">
                </div>
            </div>
        </section>
    </div>
</main>