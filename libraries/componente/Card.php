<?php

class Card{
    private $titulo;
    private $conteudo;
    private $imagem;

    function __construct($dados){
        $this->titulo = $dados->getTitulo();
        $this->conteudo = $dados->getConteudo();
        $this->imagem = $dados->getImagem();
    }

    public function getHTML(){
        $html = '<div class="card">
  <img class="card-img-top" src="'.BASEURL.''.$card['titulo'].'" alt="Card image cap">
  <div class="card-body">
    <h4 class="card-title"><a>'.$card['titulo'].'</a></h4>
    <p class="card-text">'.$card['descricao'].'</p>
    <p class="card-text">'.$card['categoria'].'</p>
  </div>
</div>';
        return $html;        
    }
}