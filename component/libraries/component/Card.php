<?php

include_once APPPATH.'libraries/util/DB.php';

class Card{
    private $arquivo;
    private $id;
    private $titulo;
    private $conteudo;
    private $categoria;

    function __construct($arquivo, $titulo, $conteudo, $categoria,$id){
        $this->arquivo = $arquivo;
        $this->titulo = $titulo;
        $this->conteudo = $conteudo;
        $this->categoria = $categoria;
        $this->id = $id;
    }
	
    public function getHTML(){
        $html = '<div class="card col-md-3 mb-3 ml-3 text-center">
                 <img class="card-img-top w-100" src="'.BASEURL.'arquivos/'.$this->categoria.'">
                    <div class="card-body">
                        <h4 class="card-title"><b>Categoria</b> - '.$this->titulo.'</h4>
                        <p class="card-text"><b>Descrição</b> - '.$this->conteudo.'</p>
                        <p class="card-text"><b>Arquivo</b> - '.$this->categoria.'</p>
                    </div>
                </div>';
        return $html;
    }
	
	    private function actionButtons($id){
        $html = '<td><a href="'.BASEURL.'upload/editar.php?id=">';
        $html .= '<i class="far fa-edit mr-3 green-text"></i></a>';
        $html .= '<td><a href="'.BASEURL.'upload/deletar.php?id=">';
        $html .= '<i class="fas fa-trash red-text"></i></a></td>';
        return $html;
    }
	
}