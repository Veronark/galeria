-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 24-Jun-2019 às 02:57
-- Versão do servidor: 10.1.36-MariaDB
-- versão do PHP: 7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `loo`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `dados_pessoais`
--

CREATE TABLE `dados_pessoais` (
  `id` int(11) NOT NULL,
  `nome` varchar(30) NOT NULL,
  `sobrenome` varchar(80) NOT NULL,
  `nascimento` varchar(10) NOT NULL,
  `estado_civil` varchar(20) NOT NULL,
  `deleted` tinyint(4) NOT NULL DEFAULT '0',
  `last_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `dados_pessoais`
--

INSERT INTO `dados_pessoais` (`id`, `nome`, `sobrenome`, `nascimento`, `estado_civil`, `deleted`, `last_modified`) VALUES
(1, 'ZÃ©', 'Maria', '12/03/1966', 'Casado', 0, '2019-05-20 18:56:23'),
(4, 'Vitor', 'Souza', '11-08-2001', 'Solteiro', 0, '2019-05-20 19:34:19'),
(5, 'Maria', 'Valente', '05/06/1954', 'ViÃºva', 0, '2019-05-20 19:36:13'),
(6, 'Bruna', 'Morais', '01/02/1998', 'Casada', 0, '2019-05-20 20:40:26'),
(7, 'Bruna', 'Morais', '01/02/1998', 'Casada', 0, '2019-05-20 20:49:51');

-- --------------------------------------------------------

--
-- Estrutura da tabela `telefone`
--

CREATE TABLE `telefone` (
  `id` int(11) NOT NULL,
  `comercial` varchar(16) NOT NULL,
  `pessoal` varchar(16) NOT NULL,
  `residencial` varchar(16) NOT NULL,
  `id_dados_pessoais` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `telefone`
--

INSERT INTO `telefone` (`id`, `comercial`, `pessoal`, `residencial`, `id_dados_pessoais`) VALUES
(1, '87953214', '35715984', '43216587', 6),
(2, '87953214', '35715984', '43216587', 7);

-- --------------------------------------------------------

--
-- Estrutura da tabela `upload`
--

CREATE TABLE `upload` (
  `id` int(11) NOT NULL,
  `nome` varchar(128) NOT NULL,
  `arquivo` varchar(64) NOT NULL,
  `descricao` varchar(200) NOT NULL,
  `categoria` varchar(32) NOT NULL,
  `deleted` tinyint(4) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `upload`
--

INSERT INTO `upload` (`id`, `nome`, `arquivo`, `descricao`, `categoria`, `deleted`, `created_at`) VALUES
(1, 'A', '20190624014306_sansundertale.jpg', 'C', 'B', 0, '2019-06-23 23:43:06'),
(2, 'Sans', '20190624023008_sansundertale.jpg', 'Personagem bem legal', 'Undertale', 0, '2019-06-24 00:30:08');

-- --------------------------------------------------------

--
-- Estrutura da tabela `usuario`
--

CREATE TABLE `usuario` (
  `id` int(11) NOT NULL,
  `nome` varchar(50) NOT NULL,
  `sobrenome` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `senha` varchar(512) NOT NULL,
  `telefone` varchar(18) NOT NULL,
  `last_modifed` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `usuario`
--

INSERT INTO `usuario` (`id`, `nome`, `sobrenome`, `email`, `senha`, `telefone`, `last_modifed`) VALUES
(3, 'Johann', 'Croife', 'jj.craque@kit.com.br', '2389492348-2', '(13) 3242-3849', '2019-03-18 20:03:37'),
(4, 'Iohana', 'Ferreira', 'iioohhanna@gmail.com', '31973210983', '(22) 8947-9348', '2019-03-18 20:15:30'),
(5, 'Sida', 'Da SIlva', 'sidinha.sidona@89bol.com.br', '439853485', '(98) 2342-2309', '2019-03-18 20:14:09'),
(6, 'Carmen', 'Pereira', 'cacazinhahh123.lolo@hotmail.com', '68425414684', '(14) 3648-5643', '2019-03-18 20:11:17'),
(7, 'Juliana', 'MarquÃªs', 'julyh.cdf33@outlook.com', '8423by283vb', '(11) 2837-4238', '2019-03-18 20:01:32'),
(8, 'Zorita', 'Walsh', 'quis@Vivamuseuismodurna.ca', '11094151', '16851012-4012', '2019-04-08 18:03:12'),
(9, 'Declan', 'Sharp', 'luctus.aliquet.odio@posuerevulputate.net', '477157599', '16160126-1066', '2019-04-08 18:03:12'),
(10, 'Emerald', 'Harmon', 'lobortis.quis.pede@fringilla.edu', '862157739', '16690219-2134', '2019-04-08 18:03:12'),
(11, 'Emily', 'Mcmahon', 'dictum.mi.ac@duiFuscealiquam.edu', '358424950', '16860219-7744', '2019-04-08 18:03:12'),
(12, 'Ivor', 'Cochran', 'Nunc.laoreet.lectus@semNulla.co.uk', '633460599', '16440409-2720', '2019-04-08 18:03:12'),
(13, 'Paloma', 'Campbell', 'tempor.diam.dictum@miDuisrisus.co.uk', '295184554', '16870829-1904', '2019-04-08 18:03:12'),
(14, 'Hadley', 'Perry', 'odio.Nam@risusat.org', '908199697', '16590803-7293', '2019-04-08 18:03:12'),
(15, 'David', 'Delgado', 'quis@Donecelementumlorem.ca', '725000960', '16280813-0245', '2019-04-08 18:03:12'),
(16, 'Perry', 'Mitchell', 'quam.quis.diam@eudui.co.uk', '230379332', '16860828-5097', '2019-04-08 18:03:12'),
(17, 'Olympia', 'Sheppard', 'ultricies.sem@fringilla.co.uk', '193372601', '16860522-6672', '2019-04-08 18:03:12'),
(18, 'Shea', 'Chen', 'mattis.velit@Integeridmagna.net', '437716021', '16431224-0007', '2019-04-08 18:03:13'),
(19, 'Gannon', 'Zimmerman', 'velit.eget@pretium.ca', '220924232', '16260626-5425', '2019-04-08 18:03:13'),
(20, 'Lareina', 'Wynn', 'et@nec.org', '908851328', '16760823-7074', '2019-04-08 18:03:13'),
(21, 'Ria', 'Villarreal', 'Duis.a.mi@liberomauris.org', '57561023', '16670411-2116', '2019-04-08 18:03:13'),
(22, 'Clark', 'Young', 'Quisque.purus@nibhenimgravida.net', '586639730', '16970924-1492', '2019-04-08 18:03:13'),
(23, 'Kiayada', 'Carrillo', 'dui.in.sodales@bibendumsed.com', '366592399', '16110813-8197', '2019-04-08 18:03:13'),
(24, 'Chaney', 'Mcdonald', 'ipsum@scelerisquesed.ca', '99949727', '16651228-1525', '2019-04-08 18:03:13'),
(25, 'Dai', 'Mooney', 'facilisis.lorem@ametornarelectus.co.uk', '711765903', '16350211-3859', '2019-04-08 18:03:13'),
(26, 'Hedy', 'Hoover', 'arcu.Aliquam.ultrices@lectussit.ca', '432890980', '16840306-5827', '2019-04-08 18:03:13'),
(27, 'Ciaran', 'Fowler', 'ut@utquamvel.edu', '675522457', '16630426-3202', '2019-04-08 18:03:13'),
(28, 'Karina', 'Hodge', 'nascetur@Quisqueornaretortor.com', '445049671', '16320610-6514', '2019-04-08 18:03:13'),
(29, 'Howard', 'Cochran', 'consectetuer@afelisullamcorper.ca', '630543068', '16430424-5667', '2019-04-08 18:03:13'),
(30, 'Rahim', 'Dunn', 'eu.odio.tristique@volutpat.net', '452421091', '16760605-3838', '2019-04-08 18:03:13'),
(31, 'Matthew', 'Bowers', 'imperdiet.dictum@liberoduinec.net', '53632565', '16141025-0268', '2019-04-08 18:03:13'),
(32, 'Abra', 'Nieves', 'sed@uterat.edu', '883185535', '16780308-5799', '2019-04-08 18:03:13'),
(33, 'Troy', 'Kinney', 'scelerisque.scelerisque.dui@nullaIntegervulputate.com', '59199763', '16760103-5129', '2019-04-08 18:03:13'),
(34, 'Harper', 'Schwartz', 'taciti@sitamet.com', '544567749', '16240908-4965', '2019-04-08 18:03:13'),
(35, 'Freya', 'Rowland', 'fringilla@ornareplaceratorci.ca', '939881467', '16940921-7495', '2019-04-08 18:03:13'),
(36, 'Gabriel', 'Cain', 'ut.dolor.dapibus@amet.com', '748985477', '16960515-6026', '2019-04-08 18:03:13'),
(37, 'Dalton', 'Bray', 'faucibus@posuerevulputatelacus.org', '187349502', '16890906-3425', '2019-04-08 18:03:13'),
(38, 'Joy', 'Munoz', 'facilisis@sit.co.uk', '914457228', '16850415-9420', '2019-04-08 18:03:13'),
(39, 'Alana', 'Clark', 'scelerisque.lorem.ipsum@ante.com', '786816024', '16230211-7276', '2019-04-08 18:03:13'),
(40, 'Angelica', 'Morrison', 'purus@eutellusPhasellus.net', '996289779', '16670227-7549', '2019-04-08 18:03:13'),
(41, 'Ramona', 'Chang', 'consequat.auctor@mi.org', '716879227', '16140615-7550', '2019-04-08 18:03:13'),
(42, 'Debra', 'Wilkerson', 'massa@fringillaornare.ca', '156454880', '16770606-4495', '2019-04-08 18:03:13'),
(43, 'Griffin', 'Campos', 'ridiculus.mus@interdum.com', '10907170', '16891013-8117', '2019-04-08 18:03:13'),
(44, 'Barrett', 'Farmer', 'a@tortorInteger.org', '514556965', '16911005-3262', '2019-04-08 18:03:13'),
(45, 'Sandra', 'Vaughn', 'eros.Nam@semper.org', '974912354', '16360417-3934', '2019-04-08 18:03:13'),
(46, 'Velma', 'Cummings', 'consequat.dolor.vitae@eulacus.ca', '161746144', '16721010-0066', '2019-04-08 18:03:13'),
(47, 'Inez', 'Lloyd', 'Etiam.vestibulum@faucibusorci.co.uk', '243217093', '16811023-6695', '2019-04-08 18:03:13');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `dados_pessoais`
--
ALTER TABLE `dados_pessoais`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `telefone`
--
ALTER TABLE `telefone`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `upload`
--
ALTER TABLE `upload`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `dados_pessoais`
--
ALTER TABLE `dados_pessoais`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `telefone`
--
ALTER TABLE `telefone`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `upload`
--
ALTER TABLE `upload`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT for table `usuario`
--
ALTER TABLE `usuario`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
