<?php

include APPPATH.'libraries/componente/Card.php';
include APPPATH.'libraries/componente/Tabela.php';
include 'libraries/UploadManager.php';

if(sizeof($_POST) > 0){
    $upload = new UploadManager();

    $data['nome'] = $_POST['titulo'];
    $data['descricao'] = $_POST['descricao'];
    $data['categoria'] = $_POST['categoria'];

    if($upload->done($data)){
        echo '<script>alert("Você conseguiu")</script>';
    }
    else{
        echo '<script>alert("Falhou")</script>';
    }
}

function lista_arquivos(){
    $upload = new UploadManager();
    $lista = $upload->lista_arquivos();
    $label = array('Número', 'Nome', 'Descrição', 'Categoria', 'Editar', 'Deletar');
    $tabela = new Tabela($lista, $label);
    return $tabela->getHTML();
}

function remove_arquivo($id){
    $upload = new UploadManager();
    if($upload->remove($id)){
        header('Location: http://localhost/b/pages/galeria.php');
    }
}

function atualiza_arquivo(){
	$upload = new UploadManager();
    if($upload->atualiza($id)){
        header('Location: http://localhost/b/pages/galeria.php');
    }
}